# AIP2019
This repository will include code resources and python-related guides for all parts of the course. If you find error, please let me know. This README will be updated as the course progresses, in principle with every lecture.

---

## Lecture 1
* Introduction to AI and agents
* Branches of AI
* Introduction to python
    * Python env setup
    * IDE and notebook setup
    * Python basics

---

## Lecture 2
* Theory about intelligent agent architecture
* Domain understanding
* Dimensions of complexity
* Representation
* Agent control architectures
	* KB, KR
	* Ontology
	* FSA
* Mini-project Pt. 1 Descriptions
* Python to Unity connection
* Feb14 special

---

## Lecture 3
* Installing python packages
* Connecting Unity to Python 2.0
* 8-puzzle game
* State graphs
* Blind and heuristic search methods

---

## Lecture 4
* Steering behaviours
* Work on mini-projects as exercises
* No relevant code (if any, might find something fun)

---

## Lecture 5
* Basic machine learning concepts
* Script for first ML tutorial

---

## Lecture 6
* Workshop - Mini project Part 1

---

## Lecture 7
* Basic ML concepts - dive into basic algorithms
* Gaussian Naive Bayes  code
* SVM code
* GMM code
* PCA code

---

## Lecture 8
* Reinforcement learning
* Cartpole balancing code 
* Transfer learning
* Multi-agent learning - Game Theory

---

## Lecture 9
* Neural networks - history
* Multi-layer perceptorns (MLPs)
* Convolutional Neural Networks (CNNs)
* Framework overview

---

## Lecture 10
* Recurrent Neural Networks
* Long-Short Term Memory Networks
* Time-series prediction

---

## Lecture 11
* Hyperparameter optimisation
* Optimizer optimisation demo script

---

## Lecture 12
* Workshop - Mini project Part 2

---
---