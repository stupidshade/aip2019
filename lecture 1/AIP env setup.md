
# Python
## Installation

### Miniconda Installation
Follow this link and your OS's instructions: https://docs.anaconda.com/anaconda/install/

**IMPORTANT: we need python 3.6 or 3.7**

After the installation we can set up a virtual environment by typing:
`conda create --name aip python=3.6` in the terminal.

This will set up a conda virtual environment using Python 3.6 as its main interpreter.

*Before the next steps I'd recommend activating the environment, navigating to the project folder and starting a jupyter notebook (`jupyter notebook` command) then opening the `"AIP env setup.ipynb"`*

### Install PyCharm EDU or Community
Download the installer of your choice from https://www.jetbrains.com/pycharm/

After installation and opening a project folder make sure to go to `File > Settings > Project: _projectName_ > Project Interpreter`
and set the interpreter to the created environment following the figures below:

#### Figure 1:
![](./img/fig1.png)

#### Figure 2:
![](./img/fig2.png)

#### Figure 3:
![](./img/fig3.png)

Finally, choose your environment's location, which (if you kept the default settings when creating the environment) should be `C:\Users\<USER>\Anaconda3\envs\<ENV NAME>`

> If you can't locate the environment, you can get its path by typing `conda env list` in a terminal.

To select the interpreter, you need to open the `ENV NAME` folder and search for `python.exe`. Once selected `Apply` and `OK` to let the IDE apply the interpreter.

### Congratulations, you're all set to code in python!
