# 01
print("Hello World!")
# Source: http://www.annedawson.net/Python3Programs.txt

# 02
the_text = input("Enter some text ")
print("This is what you entered:")
print(the_text)

# 03
# Note that \n within quote marks forces a new line to be printed
the_text = input("Enter some text\n")
print("This is what you entered:")
print(the_text)

# 04
prompt = "Enter a some text "
the_text = input(prompt)
print("This is what you entered:")
print(the_text)

# 05
total = 0.0
number1 = float(input("Enter the first number: "))
total = total + number1
number2 = float(input("Enter the second number: "))
total = total + number2
number3 = float(input("Enter the third number: "))
total = total + number3
average = total / 3
print("The average is " + str(average))

# 06
total = 0.0
count = 0
while count < 3:
    number = float(input("Enter a number: "))
    count = count + 1
    total = total + number
average = total / 3
print("The average is " + str(average))

# 07
total = 10
print(total)
print(type(total))

# 08
print(2 + 4)
print(6 - 4)
print(6 * 3)
print(6 / 3)
print(6 % 3)
print(6 // 3)  # floor division: always truncates fractional remainders
print(-5)
print(3**2)  # three to the power of 2

# 09
print(2.0 + 4.0)
print(6.0 - 4.0)
print(6.0 * 3.0)
print(6.0 / 3.0)
print(6.0 % 3.0)
print(6.0 // 3.0)  # floor division: always truncates fractional remainders
print(-5.0)
print(3.0**2.0)  # three to the power of 2

# 10
# mixing data types in expressions
# mixed type expressions are "converted up"
# converted up means to take the data type with the greater storage
# float has greater storage (8 bytes) than a regular int (4 bytes)
print(2 + 4.0)
print(6 - 4.0)
print(6 * 3.0)
print(6 / 3.0)
print(6 % 3.0)
print(6 // 3.0)  # floor division: always truncates fractional remainders
print(-5.0)
print(3**2.0)  # three to the power of 2

# 11
# these are Boolean expressions which result in a value of
# true or false
# Note that Python stores true as integer 1, and false as integer 0
# but outputs 'true' or 'false' from print statements
print(7 > 10)
print(4 < 16)
print(4 == 4)
print(4 <= 4)
print(4 >= 4)
print(4 != 4)

# 12
# these are string objects
print("Hello out there")
print('Hello')
print("Where's the spam?")
print('x')

# 13
# these are string assignments
a = "Hello out there"
print(a)
b = 'Hello'
print(b)
c = "Where's the spam?"
print(c)
d = 'x'
print(d)

# 14
a = 'Hello out there'
b = "Where's the spam?"
c = a + b
print(c)

# 15
a = 'Hello out there'
b = "Where's the spam?"
c = a + b
print(c)
d = c + 10  # < will throw an error
# you cannot concatenate a string and an integer
# you must convert the integer to a string first:
d = c + str(10)
print(d)

# 16
a = "10"
b = '99'
c = a + b
print(c)
print(type(c))
c = int(c)
print(c)
print(type(c))

# 17
# How to round up a floating point number
# to the nearest integer
# Updated: Monday 24th January 2011, 16:24 PT, AD
x = 1.6
print(x)
x = round(x)
print(x)
# compare the above with
x = 1.6
x = int(x)
print(x)

# Later: vectors, lists, tuples, dicts, matrices, iterating, string formatting
